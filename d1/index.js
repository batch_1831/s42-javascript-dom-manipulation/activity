// [SECTION] Document Object Model
// it allows us to access or modiify the properties of an element in a webpage
//it is also a standard on how to get, change, add, or delete HTML elements. 

/*
    Syntax: 
        document.querySelector("htmlElement");


    - The querySelector function takes a string input that is formatted like CSS Selector when applying styles.
*/

// Alternative way on retrieving HTML Elements

// document.getElementById("txt-first-name");
// document.getElementsByClassName("txt-last-name");

// However, using these functions requires us to identify beforehand we get the element. With querySelector, we can be flexible in how to retrieve elements. 

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners

// Whenever a user interacts with a web page, this action is considered as an event. (example: mouse click, mouse hover, page load, key press, etc.)

// addEventListener takes two arguments:
    // a string that identify an event
    // a function that the listener will execute once the "specified event" is triggered.
// txtFirstName.addEventListener("keyup", () => {
//     // "innerHTML" property sets or returns the HTML content(innerHTML) of an elements (div, spans, etc.).
//     // ".value" property sets  or returns the value of an attribute (form controls).
//     spanFullName.innerHTML = `${txtFirstName.value}`;
// });

// when event occurs, an "event object" is passed to the function argument as the first parameter.
// txtFirstName.addEventListener("keyup", (event) => {
//     // The "event.target" contains the elemenet where the event happened.
//     console.log(event.target);
//     // The "event.target.value" gets the value of the input object(txt-first-name)
//     console.log(event.target.value);
// });

// Creating Multiple event that use the same function.
const fullName = () =>{
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

//Activity

/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
	HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
	HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/
const colors = document.querySelector("#colors");


const changeColor = () => {
    spanFullName.style.color = colors.value;
}

colors.addEventListener("click", changeColor);
